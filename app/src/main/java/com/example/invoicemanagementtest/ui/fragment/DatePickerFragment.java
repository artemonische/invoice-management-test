package com.example.invoicemanagementtest.ui.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    public static void showDatePickerFragment(FragmentManager fragmentManager, Fragment targetFragment) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setTargetFragment(targetFragment, 0);
        newFragment.show(fragmentManager, "datePicker");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener listener
                = (DatePickerDialog.OnDateSetListener) getTargetFragment();

        // Create a new instance of TimePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }
}