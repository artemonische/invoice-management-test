package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.adapter.RecyclerViewEmptySupport;
import com.example.invoicemanagementtest.adapter.SearchResultRecyclerViewAdapter;
import com.example.invoicemanagementtest.controller.operation.SearchByNumberAndDateOperation;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.redmadrobot.chronos.ChronosOperationResult;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchResultFragment extends BaseFragment {

    private static final String NAME_ARGUMENT = SearchResultFragment.class.getName()
            + ".NAME_ARGUMENT";

    private static final String DATE_ARGUMENT = SearchResultFragment.class.getName()
            + ".DATE_ARGUMENT";

    private SearchResultRecyclerViewAdapter mAdapter;

    private ArrayList<BaseModel> mBaseModelArrayList;

    @Bind(R.id.recycler_view_search_result_list)
    RecyclerViewEmptySupport mRecyclerView;

    @Bind(R.id.text_view_search_empty)
    TextView mTextViewSearchResultEmpty;

    public static SearchResultFragment newInstance(String name, Date date) {

        Bundle args = new Bundle();
        args.putString(NAME_ARGUMENT, name);
        args.putSerializable(DATE_ARGUMENT, date);
        SearchResultFragment fragment = new SearchResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        setTitle(getString(R.string.search_results));

        View rootView = inflater.inflate(R.layout.fragment_search_result_list, container, false);
        ButterKnife.bind(this, rootView);

        Context context = rootView.getContext();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mBaseModelArrayList = new ArrayList<>();
        mAdapter = new SearchResultRecyclerViewAdapter(mBaseModelArrayList);
        mRecyclerView.setEmptyView(mTextViewSearchResultEmpty);
        mRecyclerView.setAdapter(mAdapter);

        String name = getArguments().getString(NAME_ARGUMENT);
        Date date = (Date) getArguments().getSerializable(DATE_ARGUMENT);

        getChronosConnector().runOperation(new SearchByNumberAndDateOperation(name, date), false);
        return rootView;
    }

    public void onOperationFinished(ChronosOperationResult chronosOperationResult) {
        if (chronosOperationResult instanceof SearchByNumberAndDateOperation.Result &&
                chronosOperationResult.isSuccessful()) {
            mBaseModelArrayList.clear();
            mBaseModelArrayList.addAll(
                    ((SearchByNumberAndDateOperation.Result) chronosOperationResult).getOutput());
            mAdapter.notifyDataSetChanged();
        } else {
            showToast(R.string.error);
        }
    }
}
