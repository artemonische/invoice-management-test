package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.adapter.PaymentRequestRecyclerViewAdapter;
import com.example.invoicemanagementtest.adapter.RecyclerViewEmptySupport;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.PaymentRequest;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaymentRequestShowFragment extends BaseFragment {

    private OnPaymentRequestInteractionListener mListener;

    @Bind(R.id.recycler_view_payment_request_list)
    RecyclerViewEmptySupport mRecyclerView;

    @Bind(R.id.text_view_payment_request_list_empty)
    TextView mTextViewPaymentRequestEmpty;

    public interface OnPaymentRequestInteractionListener {

        void onPaymentRequestEditInteraction(PaymentRequest paymentRequest);
    }

    public static PaymentRequestShowFragment newInstance() {
        return new PaymentRequestShowFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        setTitle(getString(R.string.payment_requests));

        View rootView = inflater.inflate(R.layout.fragment_payment_request_show, container, false);
        ButterKnife.bind(this, rootView);
        Context context = rootView.getContext();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mListener = new OnPaymentRequestInteractionListener() {
            @Override
            public void onPaymentRequestEditInteraction(final PaymentRequest paymentRequest) {
                showFragmentBackStack(
                        PaymentRequestCreateUpdateFragment.newInstance(paymentRequest));
            }
        };
        mRecyclerView.setEmptyView(mTextViewPaymentRequestEmpty);
        mRecyclerView.setAdapter(new PaymentRequestRecyclerViewAdapter(
                Database.getInstance().getPaymentRequestList(), mListener));

        FloatingActionButton floatingActionButton = (FloatingActionButton) rootView
                .findViewById(R.id.floating_button_add_payment_request);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                showFragmentBackStack(PaymentRequestCreateUpdateFragment.newInstance());
            }
        });
        return rootView;
    }
}
