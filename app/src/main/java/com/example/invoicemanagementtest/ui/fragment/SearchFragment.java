package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener {

    @Bind(R.id.edit_text_number)
    EditText mEditTextNumber;

    @Bind(R.id.button_date)
    Button mButtonDate;

    private Date mDateSelected;

    @Bind(R.id.button_search)
    Button mButtonSearch;

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, rootView);

        setTitle(getString(R.string.search));

        return rootView;
    }

    @Override
    public void onDateSet(final DatePicker view, final int year, final int monthOfYear,
            final int dayOfMonth) {
        mDateSelected = new Date(year, monthOfYear, dayOfMonth);
        mButtonDate.setText(year + " " + monthOfYear + " " + dayOfMonth);
    }

    @OnClick(R.id.button_date)
    void onSelectDateClick() {
        DatePickerFragment.showDatePickerFragment(getSupportFragmentManager(), this);
    }

    @OnClick(R.id.button_search)
    void onSearchClick() {
        boolean isSearchCorrect = !TextUtils.isEmpty(mEditTextNumber.getText()) ||
                mDateSelected != null;
        if (isSearchCorrect) {
            showFragmentBackStack(SearchResultFragment
                    .newInstance(mEditTextNumber.getText().toString(), mDateSelected));
        } else {
            showToast(R.string.at_least_one_data_must_be_entered);
        }
    }
}
