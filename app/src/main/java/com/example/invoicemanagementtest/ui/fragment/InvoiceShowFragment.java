package com.example.invoicemanagementtest.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.adapter.InvoiceRecyclerViewAdapter;
import com.example.invoicemanagementtest.adapter.RecyclerViewEmptySupport;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.Invoice;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InvoiceShowFragment extends BaseFragment {

    private OnInvoiceInteractionListener mListener;

    @Bind(R.id.recycler_view_invoice_list)
    RecyclerViewEmptySupport mRecyclerView;

    @Bind(R.id.text_view_invoice_list_empty)
    TextView mTextViewInvoiceListEmpty;

    @Bind(R.id.floating_button_add_invoice)
    FloatingActionButton mFloatingActionButton;

    public interface OnInvoiceInteractionListener {

        void onInvoiceEditInteraction(Invoice invoice);
    }

    public static InvoiceShowFragment newInstance() {
        return new InvoiceShowFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        setTitle(getString(R.string.invoices));

        View rootView = inflater.inflate(R.layout.fragment_invoice_show, container, false);
        ButterKnife.bind(this, rootView);
        Context context = rootView.getContext();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mListener = new OnInvoiceInteractionListener() {
            @Override
            public void onInvoiceEditInteraction(final Invoice invoice) {
                showFragmentBackStack(InvoiceCreateUpdateFragment.newInstance(invoice));
            }
        };
        mRecyclerView.setEmptyView(mTextViewInvoiceListEmpty);
        mRecyclerView.setAdapter(
                new InvoiceRecyclerViewAdapter(Database.getInstance().getInvoiceList(),
                        mListener));

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                showFragmentBackStack(InvoiceCreateUpdateFragment.newInstance());
            }
        });
        return rootView;
    }
}
