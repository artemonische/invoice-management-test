package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.Invoice;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InvoiceCreateUpdateFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener {

    private static final String INVOICE_ARGUMENT = InvoiceCreateUpdateFragment.class.getName()
            + ".INVOICE_ARGUMENT";

    private boolean isEditMode = false;

    @Bind(R.id.edit_text_number)
    EditText mEditTextNumber;

    @Bind(R.id.button_date)
    Button mButtonDate;

    private Date mDateSelected;

    @Bind(R.id.edit_text_user)
    EditText mEditTextUser;

    @Bind(R.id.edit_text_sum)
    EditText mEditTextSum;

    @Bind(R.id.edit_text_currency)
    EditText mEditTextCurrency;

    @Bind(R.id.edit_text_currency_rate)
    EditText mEditTextCurrencyRate;

    @Bind(R.id.edit_text_product)
    EditText mEditTextProduct;

    @Bind(R.id.edit_text_quantity)
    EditText mEditTextQuantity;

    @Bind(R.id.button_add_invoice)
    Button mButtonAddInvoice;

    private Invoice mInvoiceForEdit;

    public static InvoiceCreateUpdateFragment newInstance(Invoice invoice) {
        Bundle args = new Bundle();
        args.putSerializable(INVOICE_ARGUMENT, invoice);
        InvoiceCreateUpdateFragment fragment = new InvoiceCreateUpdateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static InvoiceCreateUpdateFragment newInstance() {
        return new InvoiceCreateUpdateFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_invoice_create_update, container, false);
        ButterKnife.bind(this, rootView);

        mInvoiceForEdit = null;
        if (getArguments() != null) {
            mInvoiceForEdit = (Invoice) getArguments().getSerializable(INVOICE_ARGUMENT);
        }
        isEditMode = mInvoiceForEdit != null;
        if (isEditMode) {
            mEditTextNumber.setText(mInvoiceForEdit.getNumber());
            mDateSelected = mInvoiceForEdit.getDate();
            mButtonDate.setText(
                    mDateSelected.getYear() + " " + mDateSelected.getMonth() + " "
                            + mDateSelected.getDate());
            mEditTextUser.setText(mInvoiceForEdit.getUser());
            mEditTextSum.setText(String.valueOf(mInvoiceForEdit.getSum()));
            mEditTextCurrency.setText(mInvoiceForEdit.getCurrency());
            mEditTextCurrencyRate.setText(String.valueOf(mInvoiceForEdit.getCurrencyRate()));
            mEditTextProduct.setText(mInvoiceForEdit.getProduct());
            mEditTextQuantity.setText(String.valueOf(mInvoiceForEdit.getQuantity()));
            mButtonAddInvoice.setText(R.string.change_invoice);
            setTitle(getString(R.string.updating_invoice));
        } else {
            setTitle(getString(R.string.adding_invoice));
        }

        return rootView;
    }

    @Override
    public void onDateSet(final DatePicker view, final int year, final int monthOfYear,
            final int dayOfMonth) {
        mDateSelected = new Date(year, monthOfYear, dayOfMonth);
        mButtonDate.setText(year + " " + monthOfYear + " " + dayOfMonth);
    }

    @OnClick(R.id.button_date)
    void onSelectDateClick() {
        DatePickerFragment.showDatePickerFragment(getSupportFragmentManager(), this);
    }

    @OnClick(R.id.button_add_invoice)
    void onAddInvoiceClick() {
        boolean isInvoiceCorrect = !TextUtils.isEmpty(mEditTextNumber.getText()) &&
                mDateSelected != null &&
                !TextUtils.isEmpty(mEditTextUser.getText()) &&
                !TextUtils.isEmpty(mEditTextSum.getText()) &&
                !TextUtils.isEmpty(mEditTextCurrency.getText()) &&
                !TextUtils.isEmpty(mEditTextCurrencyRate.getText()) &&
                !TextUtils.isEmpty(mEditTextProduct.getText()) &&
                !TextUtils.isEmpty(mEditTextQuantity.getText());
        if (isInvoiceCorrect) {
            Invoice invoice;
            if (isEditMode) {
                invoice = mInvoiceForEdit;
            } else {
                invoice = new Invoice();
            }
            try {
                invoice.setNumber(mEditTextNumber.getText().toString());
                if (mDateSelected != null) {
                    invoice.setDate(mDateSelected);
                }
                invoice.setUser(mEditTextUser.getText().toString());
                invoice.setSum(Double.valueOf(mEditTextSum.getText().toString()));
                invoice.setCurrency(mEditTextCurrency.getText().toString());
                invoice.setCurrencyRate(Double.valueOf(mEditTextCurrencyRate.getText().toString()));
                invoice.setProduct(mEditTextProduct.getText().toString());
                invoice.setQuantity(Double.valueOf(mEditTextQuantity.getText().toString()));

                if (isEditMode) {
                    Database.getInstance().updateInvoice(invoice);
                    showToast(R.string.invoice_successfully_changed);
                } else {
                    Database.getInstance().addInvoice(invoice);
                    showToast(R.string.invoice_successfully_added);
                }
            } catch (NumberFormatException exception) {
                showToast(R.string.wrong_entered_number);
            }
        } else {
            showToast(R.string.all_data_must_be_entered);
        }
    }
}
