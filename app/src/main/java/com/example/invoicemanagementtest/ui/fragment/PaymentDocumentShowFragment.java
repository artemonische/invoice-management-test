package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.adapter.PaymentDocumentRecyclerViewAdapter;
import com.example.invoicemanagementtest.adapter.RecyclerViewEmptySupport;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaymentDocumentShowFragment extends BaseFragment {

    private OnPaymentDocumentInteractionListener mListener;

    @Bind(R.id.recycler_view_payment_document_list)
    RecyclerViewEmptySupport mRecyclerView;

    @Bind(R.id.text_view_payment_document_list_empty)
    TextView mTextViewPaymentDocumentListEmpty;

    @Bind(R.id.floating_button_add_payment_document)
    FloatingActionButton mFloatingActionButton;

    public interface OnPaymentDocumentInteractionListener {

        void onPaymentDocumentEditInteraction(final PaymentDocument paymentDocument);
    }

    public static PaymentDocumentShowFragment newInstance() {
        return new PaymentDocumentShowFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        setTitle(getString(R.string.payment_documents));

        View rootView = inflater.inflate(R.layout.fragment_payment_document_show, container, false);
        ButterKnife.bind(this, rootView);
        Context context = rootView.getContext();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mListener = new OnPaymentDocumentInteractionListener() {
            @Override
            public void onPaymentDocumentEditInteraction(final PaymentDocument paymentDocument) {
                showFragmentBackStack(
                        PaymentDocumentCreateUpdateFragment.newInstance(paymentDocument));
            }
        };
        mRecyclerView.setEmptyView(mTextViewPaymentDocumentListEmpty);
        mRecyclerView.setAdapter(
                new PaymentDocumentRecyclerViewAdapter(
                        Database.getInstance().getPaymentDocumentList(), mListener));

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                showFragmentBackStack(PaymentDocumentCreateUpdateFragment.newInstance());
            }
        });
        return rootView;
    }
}
