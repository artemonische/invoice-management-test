package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentDocumentCreateUpdateFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener {

    private static final String PAYMENT_DOCUMENT_ARGUMENT =
            PaymentDocumentCreateUpdateFragment.class.getName()
                    + ".PAYMENT_DOCUMENT_ARGUMENT";

    @Bind(R.id.edit_text_number)
    EditText mEditTextNumber;

    @Bind(R.id.button_date)
    Button mButtonDate;

    @Bind(R.id.edit_text_user)
    EditText mEditTextUser;

    @Bind(R.id.edit_text_sum)
    EditText mEditTextSum;

    @Bind(R.id.edit_text_employeer)
    EditText mEditTextEmployeer;

    @Bind(R.id.button_add_payment_document)
    Button mButtonAddPaymentDocument;

    private boolean isEditMode = false;

    private Date mDateSelected;

    private PaymentDocument mPaymentDocumentForEdit;

    public static PaymentDocumentCreateUpdateFragment newInstance(PaymentDocument paymentDocument) {
        Bundle args = new Bundle();
        args.putSerializable(PAYMENT_DOCUMENT_ARGUMENT, paymentDocument);
        PaymentDocumentCreateUpdateFragment fragment = new PaymentDocumentCreateUpdateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static PaymentDocumentCreateUpdateFragment newInstance() {
        return new PaymentDocumentCreateUpdateFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater
                .inflate(R.layout.fragment_payment_document_create_update, container, false);
        ButterKnife.bind(this, rootView);

        mPaymentDocumentForEdit = null;
        if (getArguments() != null) {
            mPaymentDocumentForEdit = (PaymentDocument) getArguments()
                    .getSerializable(PAYMENT_DOCUMENT_ARGUMENT);
        }
        isEditMode = mPaymentDocumentForEdit != null;
        if (isEditMode) {
            mEditTextNumber.setText(mPaymentDocumentForEdit.getNumber());
            mDateSelected = mPaymentDocumentForEdit.getDate();
            mButtonDate.setText(
                    mDateSelected.getYear() + " " + mDateSelected.getMonth() + " "
                            + mDateSelected.getDate());
            mEditTextUser.setText(mPaymentDocumentForEdit.getUser());
            mEditTextSum.setText(String.valueOf(mPaymentDocumentForEdit.getSum()));
            mEditTextEmployeer.setText(mPaymentDocumentForEdit.getEmployeer());
            mButtonAddPaymentDocument.setText(R.string.update_payment_document);
            setTitle(getString(R.string.updating_payment_document));
        } else {
            setTitle(getString(R.string.adding_payment_document));
        }

        return rootView;
    }

    @Override
    public void onDateSet(final DatePicker view, final int year, final int monthOfYear,
            final int dayOfMonth) {
        mDateSelected = new Date(year, monthOfYear, dayOfMonth);
        mButtonDate.setText(year + " " + monthOfYear + " " + dayOfMonth);
    }

    @OnClick(R.id.button_date)
    void onSelectDateClick() {
        DatePickerFragment.showDatePickerFragment(getSupportFragmentManager(), this);
    }

    @OnClick(R.id.button_add_payment_document)
    void onAddInvoiceClick() {
        boolean isPaymentDocumentCorrect = !TextUtils.isEmpty(mEditTextNumber.getText()) &&
                mDateSelected != null &&
                !TextUtils.isEmpty(mEditTextUser.getText()) &&
                !TextUtils.isEmpty(mEditTextSum.getText()) &&
                !TextUtils.isEmpty(mEditTextEmployeer.getText());
        if (isPaymentDocumentCorrect) {
            PaymentDocument paymentDocument;
            if (isEditMode) {
                paymentDocument = mPaymentDocumentForEdit;
            } else {
                paymentDocument = new PaymentDocument();
            }
            try {
                paymentDocument.setNumber(mEditTextNumber.getText().toString());
                if (mDateSelected != null) {
                    paymentDocument.setDate(mDateSelected);
                }
                paymentDocument.setUser(mEditTextUser.getText().toString());
                paymentDocument.setSum(Double.valueOf(mEditTextSum.getText().toString()));
                paymentDocument.setEmployeer(mEditTextEmployeer.getText().toString());
                paymentDocument.save();

                if (isEditMode) {
                    Database.getInstance().updatePaymentDocument(paymentDocument);
                    showToast(R.string.payment_document_successfully_changed);
                } else {
                    Database.getInstance().addPaymentDocument(paymentDocument);
                    showToast(R.string.payment_document_successfully_added);
                }
            } catch (NumberFormatException exception) {
                showToast(R.string.wrong_entered_number);
            }
        } else {
            showToast(R.string.all_data_must_be_entered);
        }
    }
}
