package com.example.invoicemanagementtest.ui.fragment;

import com.example.invoicemanagementtest.ui.activity.MainActivity;
import com.redmadrobot.chronos.ChronosConnector;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseFragment extends Fragment {

    protected ChronosConnector mChronosConnector = new ChronosConnector();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChronosConnector.onCreate(this, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mChronosConnector.onResume();
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        mChronosConnector.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        mChronosConnector.onPause();
        super.onPause();
    }

    protected ChronosConnector getChronosConnector() {
        return mChronosConnector;
    }

    protected void showFragment(BaseFragment baseFragment) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showFragment(baseFragment);
        }
    }

    protected void showFragmentBackStack(BaseFragment baseFragment) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showFragmentBackstack(baseFragment);
        }
    }

    protected void setTitle(String title) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setTitle(title);
        }
    }

    protected FragmentManager getSupportFragmentManager() {
        if (getActivity() instanceof AppCompatActivity) {
            return ((AppCompatActivity) getActivity()).getSupportFragmentManager();
        } else {
            return null;
        }
    }

    protected void showToast(int messageId) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showToast(messageId);
        }
    }

}
