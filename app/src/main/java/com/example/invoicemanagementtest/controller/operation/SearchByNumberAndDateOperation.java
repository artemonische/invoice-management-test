package com.example.invoicemanagementtest.controller.operation;

import com.example.invoicemanagementtest.db.Database;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchByNumberAndDateOperation extends ChronosOperation<List<BaseModel>> {

    private Date mDate;

    private String mName;

    public SearchByNumberAndDateOperation(@Nullable String name, @Nullable Date date) {
        mDate = date;
        mName = name;
    }

    @Nullable
    @Override
    public List<BaseModel> run() {
        List<BaseModel> resultList = new ArrayList<>();
        Set<BaseModel> baseModelSet = new HashSet<>();
        if (!TextUtils.isEmpty(mName)) {
            baseModelSet.addAll(Database.getInstance().searchByNumber(mName));
        }
        if (mDate != null) {
            baseModelSet.addAll(Database.getInstance().searchByDate(mDate));
        }
        resultList.addAll(baseModelSet);
        return resultList;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<List<BaseModel>>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<List<BaseModel>> {

    }
}
