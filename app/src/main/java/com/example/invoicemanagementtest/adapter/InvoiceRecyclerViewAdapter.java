package com.example.invoicemanagementtest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.Invoice;
import com.example.invoicemanagementtest.ui.fragment.InvoiceShowFragment.OnInvoiceInteractionListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InvoiceRecyclerViewAdapter
        extends RecyclerViewEmptySupport.Adapter<InvoiceRecyclerViewAdapter.ViewHolder> {

    private final List<Invoice> mInvoiceList;

    private final OnInvoiceInteractionListener mListener;

    public InvoiceRecyclerViewAdapter(List<Invoice> items,
            OnInvoiceInteractionListener listener) {
        mInvoiceList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_invoice_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mInvoiceList.get(position);
        holder.mIdView.setText(String.valueOf(mInvoiceList.get(position).getId()));
        holder.mContentView.setText(mInvoiceList.get(position).toString());
        holder.mButtonUpdateInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mListener.onInvoiceEditInteraction(mInvoiceList.get(position));
            }
        });
        holder.mButtonDeleteInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Database.getInstance().deleteInvoice(mInvoiceList.get(position));
                mInvoiceList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mInvoiceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        @Bind(R.id.text_view_invoice_id)
        TextView mIdView;

        @Bind(R.id.text_view_invoice_content)
        TextView mContentView;

        @Bind(R.id.button_update_invoice)
        Button mButtonUpdateInvoice;

        @Bind(R.id.button_delete_invoice)
        Button mButtonDeleteInvoice;

        public Invoice mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }
}
