package com.example.invoicemanagementtest.adapter;

import com.example.invoicemanagementtest.R;
import com.raizlabs.android.dbflow.structure.BaseModel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchResultRecyclerViewAdapter
        extends RecyclerViewEmptySupport.Adapter<SearchResultRecyclerViewAdapter.ViewHolder> {

    private final List<BaseModel> mBaseModelList;

    public SearchResultRecyclerViewAdapter(List<BaseModel> items) {
        mBaseModelList = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_search_result_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mBaseModelList.get(position);
        holder.mContentView.setText(mBaseModelList.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return mBaseModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        @Bind(R.id.text_view_search_result_content)
        TextView mContentView;

        public BaseModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }
}
