package com.example.invoicemanagementtest.adapter;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.PaymentRequest;
import com.example.invoicemanagementtest.ui.fragment.PaymentRequestShowFragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaymentRequestRecyclerViewAdapter
        extends RecyclerViewEmptySupport.Adapter<PaymentRequestRecyclerViewAdapter.ViewHolder> {

    private final List<PaymentRequest> mPaymentRequestList;

    private final PaymentRequestShowFragment.OnPaymentRequestInteractionListener mListener;

    public PaymentRequestRecyclerViewAdapter(List<PaymentRequest> items,
            PaymentRequestShowFragment.OnPaymentRequestInteractionListener listener) {
        mPaymentRequestList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_payment_request_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mPaymentRequestList.get(position);
        holder.mIdView.setText(String.valueOf(mPaymentRequestList.get(position).getId()));
        holder.mContentView.setText(mPaymentRequestList.get(position).toString());
        holder.mButtonUpdatePaymentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mListener.onPaymentRequestEditInteraction(mPaymentRequestList.get(position));
            }
        });
        holder.mButtonDeletePaymentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Database.getInstance().deletePaymentRequest(mPaymentRequestList.get(position));
                mPaymentRequestList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPaymentRequestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        @Bind(R.id.text_view_payment_request_id)
        TextView mIdView;

        @Bind(R.id.text_view_payment_request_content)
        TextView mContentView;

        @Bind(R.id.button_update_payment_request)
        Button mButtonUpdatePaymentRequest;

        @Bind(R.id.button_delete_payment_request)
        Button mButtonDeletePaymentRequest;

        public PaymentRequest mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }
}
