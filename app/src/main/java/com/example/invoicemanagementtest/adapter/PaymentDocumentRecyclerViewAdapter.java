package com.example.invoicemanagementtest.adapter;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument;
import com.example.invoicemanagementtest.ui.fragment.PaymentDocumentShowFragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaymentDocumentRecyclerViewAdapter
        extends RecyclerViewEmptySupport.Adapter<PaymentDocumentRecyclerViewAdapter.ViewHolder> {

    private final List<PaymentDocument> mPaymentDocumentList;

    private final PaymentDocumentShowFragment.OnPaymentDocumentInteractionListener mListener;

    public PaymentDocumentRecyclerViewAdapter(List<PaymentDocument> items,
            PaymentDocumentShowFragment.OnPaymentDocumentInteractionListener listener) {
        mPaymentDocumentList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_payment_document_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mPaymentDocumentList.get(position);
        holder.mIdView.setText(String.valueOf(mPaymentDocumentList.get(position).getId()));
        holder.mContentView.setText(mPaymentDocumentList.get(position).toString());
        holder.mButtonUpdatePaymentDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mListener.onPaymentDocumentEditInteraction(mPaymentDocumentList.get(position));
            }
        });
        holder.mButtonDeletePaymentDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Database.getInstance().deletePaymentDocument(mPaymentDocumentList.get(position));
                mPaymentDocumentList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPaymentDocumentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        @Bind(R.id.text_view_payment_document_id)
        TextView mIdView;

        @Bind(R.id.text_view_payment_document_content)
        TextView mContentView;

        @Bind(R.id.button_update_payment_document)
        Button mButtonUpdatePaymentDocument;

        @Bind(R.id.button_delete_payment_document)
        Button mButtonDeletePaymentDocument;

        public PaymentDocument mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }
}
