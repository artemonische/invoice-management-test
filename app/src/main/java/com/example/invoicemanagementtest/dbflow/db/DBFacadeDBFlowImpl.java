package com.example.invoicemanagementtest.dbflow.db;

import com.example.invoicemanagementtest.db.DBFacade;
import com.example.invoicemanagementtest.dbflow.table.Invoice;
import com.example.invoicemanagementtest.dbflow.table.Invoice_Table;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument_Table;
import com.example.invoicemanagementtest.dbflow.table.PaymentRequest;
import com.example.invoicemanagementtest.dbflow.table.PaymentRequest_Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBFacadeDBFlowImpl implements DBFacade {

    @Override
    public List<Invoice> getInvoiceList() {
        return new Select().from(Invoice.class).queryList();
    }

    @Override
    public List<PaymentDocument> getPaymentDocumentList() {
        return new Select().from(PaymentDocument.class).queryList();
    }

    @Override
    public List<PaymentRequest> getPaymentRequestList() {
        return new Select().from(PaymentRequest.class).queryList();
    }

    @Override
    public void addInvoice(final Invoice invoice) {
        invoice.save();
    }

    @Override
    public void addPaymentDocument(final PaymentDocument paymentDocument) {
        paymentDocument.save();
    }

    @Override
    public void addPaymentRequest(final PaymentRequest paymentRequest) {
        paymentRequest.save();
    }

    @Override
    public void updateInvoice(final Invoice invoice) {
        invoice.save();
    }

    @Override
    public void updatePaymentDocument(final PaymentDocument paymentDocument) {
        paymentDocument.save();
    }

    @Override
    public void updatePaymentRequest(final PaymentRequest paymentRequest) {
        paymentRequest.save();
    }

    @Override
    public void deleteInvoice(final Invoice invoice) {
        invoice.delete();
    }

    @Override
    public void deletePaymentDocument(final PaymentDocument paymentDocument) {
        paymentDocument.delete();
    }

    @Override
    public void deletePaymentRequest(final PaymentRequest paymentRequest) {
        paymentRequest.delete();
    }

    @Override
    public List<BaseModel> searchByNumber(final String name) {
        List<BaseModel> resultList = new ArrayList<>();
        List<Invoice> invoiceList = new Select().
                from(Invoice.class).
                where(Invoice_Table.number.like("%" + name + "%")).
                queryList();
        resultList.addAll(invoiceList);
        List<PaymentDocument> paymentDocumentList = new Select().
                from(PaymentDocument.class).
                where(PaymentDocument_Table.number.like("%" + name + "%")).
                queryList();
        resultList.addAll(paymentDocumentList);
        List<PaymentRequest> paymentRequestList = new Select().
                from(PaymentRequest.class).
                where(PaymentRequest_Table.number.like("%" + name + "%")).
                queryList();
        resultList.addAll(paymentRequestList);
        return resultList;
    }

    @Override
    public List<BaseModel> searchByDate(final Date date) {
        List<BaseModel> resultList = new ArrayList<>();
        List<Invoice> invoiceList = new Select().
                from(Invoice.class).
                where(Invoice_Table.date.eq(date)).
                queryList();
        resultList.addAll(invoiceList);
        List<PaymentDocument> paymentDocumentList = new Select().
                from(PaymentDocument.class).
                where(PaymentDocument_Table.date.eq(date)).
                queryList();
        resultList.addAll(paymentDocumentList);
        List<PaymentRequest> paymentRequestList = new Select().
                from(PaymentRequest.class).
                where(PaymentRequest_Table.date.eq(date)).
                queryList();
        resultList.addAll(paymentRequestList);
        return resultList;
    }
}
