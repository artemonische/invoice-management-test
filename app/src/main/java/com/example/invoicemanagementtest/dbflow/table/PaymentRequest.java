package com.example.invoicemanagementtest.dbflow.table;

import com.example.invoicemanagementtest.dbflow.db.DocumentsDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.util.Date;

@ModelContainer
@Table(database = DocumentsDatabase.class)
public class PaymentRequest extends BaseModel implements Serializable {

    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String number;

    @Column
    Date date;

    @Column
    String user;

    @Column
    String contractor;

    @Column
    Double sum;

    @Column
    String currency;

    @Column
    Double currencyRate;

    @Column
    Double commission;

    public long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public String getContractor() {
        return contractor;
    }

    public Double getSum() {
        return sum;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getCurrencyRate() {
        return currencyRate;
    }

    public Double getCommission() {
        return commission;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public void setContractor(final String contractor) {
        this.contractor = contractor;
    }

    public void setSum(final Double sum) {
        this.sum = sum;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public void setCurrencyRate(final Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    public void setCommission(final Double commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "Номер: " + number + "\n" +
                "Дата: " + date + "\n" +
                "Пользователь: " + user + "\n" +
                "Контрагент: " + contractor + "\n" +
                "Сумма: " + sum + "\n" +
                "Валюта: " + currency + "\n" +
                "Курс валюты: " + currencyRate + "\n" +
                "Комиссия: " + commission;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentRequest that = (PaymentRequest) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
