package com.example.invoicemanagementtest.dbflow.table;

import com.example.invoicemanagementtest.dbflow.db.DocumentsDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.util.Date;

@ModelContainer
@Table(database = DocumentsDatabase.class)
public class Invoice extends BaseModel implements Serializable {

    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String number;

    @Column
    Date date;

    @Column
    String user;

    @Column
    Double sum;

    @Column
    String currency;

    @Column
    Double currencyRate;

    @Column
    String product;

    @Column
    Double quantity;

    public long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public Double getSum() {
        return sum;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getCurrencyRate() {
        return currencyRate;
    }

    public String getProduct() {
        return product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public void setSum(final Double sum) {
        this.sum = sum;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public void setCurrencyRate(final Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    public void setProduct(final String product) {
        this.product = product;
    }

    public void setQuantity(final Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Номер: " + number + "\n" +
                "Дата: " + date + "\n" +
                "Пользователь: " + user + "\n" +
                "Сумма: " + sum + "\n" +
                "Валюта: " + currency + "\n" +
                "Курс валюты: " + currencyRate + "\n" +
                "Продукт: " + product + "\n" +
                "Количество: " + quantity;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Invoice invoice = (Invoice) o;

        return id == invoice.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
