package com.example.invoicemanagementtest.dbflow.table;

import com.example.invoicemanagementtest.dbflow.db.DocumentsDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.util.Date;

@ModelContainer
@Table(database = DocumentsDatabase.class)
public class PaymentDocument extends BaseModel implements Serializable {

    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String number;

    @Column
    Date date;

    @Column
    String user;

    @Column
    Double sum;

    @Column
    String employeer;

    public long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public Double getSum() {
        return sum;
    }

    public String getEmployeer() {
        return employeer;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public void setSum(final Double sum) {
        this.sum = sum;
    }

    public void setEmployeer(final String employeer) {
        this.employeer = employeer;
    }

    @Override
    public String toString() {
        return "Номер: " + number + "\n" +
                "Дата: " + date + "\n" +
                "Пользователь: " + user + "\n" +
                "Сумма: " + sum + "\n" +
                "Сотрудник: " + employeer;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentDocument that = (PaymentDocument) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
