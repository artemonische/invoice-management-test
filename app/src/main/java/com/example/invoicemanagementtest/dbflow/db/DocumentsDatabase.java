package com.example.invoicemanagementtest.dbflow.db;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DocumentsDatabase.NAME, version = DocumentsDatabase.VERSION)
public class DocumentsDatabase {

    public static final String NAME = "Documents";

    public static final int VERSION = 1;
}
