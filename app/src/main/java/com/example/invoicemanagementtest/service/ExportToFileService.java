package com.example.invoicemanagementtest.service;

import com.example.invoicemanagementtest.R;
import com.example.invoicemanagementtest.db.Database;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ExportToFileService extends IntentService {

    public static final int NOTIFICATION_ID_PROGRESS = 1;

    public static final int NOTIFICATION_ID_FINISHED = 1;

    public static final String DIVIDER = "\n-----\n";

    public static final String FILE_EXTENTION = ".txt";

    public ExportToFileService() {
        super(ExportToFileService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        File file = createFile();

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.export_to_database))
                .setContentText(file.getAbsolutePath())
                .setProgress(0, 0, true)
                .setOngoing(true);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(NOTIFICATION_ID_PROGRESS, mNotifyBuilder.build());

        if (isExternalStorageWritable()) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file));
                writeDatabaseDataToFile(writer);

                mNotifyManager.cancel(NOTIFICATION_ID_PROGRESS);
                mNotifyBuilder
                        .setContentText(getString(R.string.export_to_database_ended))
                        .setProgress(0, 0, false)
                        .setOngoing(false);
                mNotifyManager.notify(0, mNotifyBuilder.build());
            } catch (IOException e) {
                mNotifyBuilder
                        .setContentText(getString(R.string.export_to_database_error))
                        .setProgress(0, 0, false)
                        .setOngoing(false);
                mNotifyManager.notify(NOTIFICATION_ID_FINISHED, mNotifyBuilder.build());
                e.printStackTrace();
            } finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            mNotifyBuilder
                    .setContentText(getString(R.string.export_to_database_error))
                    .setProgress(0, 0, false)
                    .setOngoing(false);
            mNotifyManager.notify(0, mNotifyBuilder.build());
        }
    }

    @NonNull
    private void writeDatabaseDataToFile(BufferedWriter writer) throws IOException {
        writer.write(DIVIDER + getString(R.string.invoices) + DIVIDER);
        writer.write(Database.getInstance().getInvoiceList().toString());

        writer.write(DIVIDER + getString(R.string.payment_documents) + DIVIDER);
        writer.write(Database.getInstance().getPaymentDocumentList().toString());

        writer.write(DIVIDER + getString(R.string.payment_requests) + DIVIDER);
        writer.write(Database.getInstance().getPaymentRequestList().toString());
    }

    @NonNull
    private File createFile() {
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" +
                getString(R.string.app_name));
        sdPath.mkdir();
        return new File(sdPath,
                getString(R.string.app_name) + FILE_EXTENTION);
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

}
