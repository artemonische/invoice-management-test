package com.example.invoicemanagementtest;

import com.raizlabs.android.dbflow.config.FlowManager;

import android.app.Application;

public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
}
