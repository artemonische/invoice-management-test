package com.example.invoicemanagementtest.db;

import com.example.invoicemanagementtest.dbflow.db.DBFacadeDBFlowImpl;

public final class Database {

    private static DBFacade mFacade;

    private Database() {

    }

    public static synchronized DBFacade getInstance() {
        if (mFacade == null) {
            mFacade = new DBFacadeDBFlowImpl();
        }
        return mFacade;
    }
}
