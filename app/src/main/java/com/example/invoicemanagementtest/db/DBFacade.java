package com.example.invoicemanagementtest.db;

import com.example.invoicemanagementtest.dbflow.table.Invoice;
import com.example.invoicemanagementtest.dbflow.table.PaymentDocument;
import com.example.invoicemanagementtest.dbflow.table.PaymentRequest;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

public interface DBFacade {

    List<Invoice> getInvoiceList();

    List<PaymentDocument> getPaymentDocumentList();

    List<PaymentRequest> getPaymentRequestList();

    void addInvoice(Invoice invoice);

    void addPaymentDocument(PaymentDocument paymentDocument);

    void addPaymentRequest(PaymentRequest paymentRequest);

    void updateInvoice(Invoice invoice);

    void updatePaymentDocument(PaymentDocument paymentDocument);

    void updatePaymentRequest(PaymentRequest paymentRequest);

    void deleteInvoice(Invoice invoice);

    void deletePaymentDocument(PaymentDocument paymentDocument);

    void deletePaymentRequest(PaymentRequest paymentRequest);

    List<BaseModel> searchByNumber(final String name);

    List<BaseModel> searchByDate(final Date date);
}
